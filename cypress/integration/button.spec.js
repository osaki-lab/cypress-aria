describe('aria for button', () => {
    beforeEach(() => {
        cy.visit('cypress/integration/button.html');
    });

    it('can detect <button> by "button" selector: without role', () => {
        cy.aria('button').first().click();
        cy.get('#status').contains('test1');
    });

    it('can detect <button> by "button" selector by caption', () => {
        cy.aria('button.test2').first().click();
        cy.get('#status').contains('test2');
    });

    it('can detect <input type="button"> by "button" selector by name attr', () => {
        cy.aria('button.test3').first().click();
        cy.get('#status').contains('test3');
    });

    it('can detect <input type="button"> by "button" selector by value attr', () => {
        cy.aria('button.test4').first().click();
        cy.get('#status').contains('test4');
    });

    it('can detect <input type="submit"> by "button" selector by value attr', () => {
        cy.aria('button.test5').first().click();
        cy.get('#status').contains('test5');
    });

    it('can detect <input type="reset"> by "button" selector by value attr', () => {
        cy.aria('button.test6').first().click();
        cy.get('#status').contains('test6');
    });

    it('can detect <input type="image"> by "button" selector by alt attr', () => {
        cy.aria('button.test7').first().click();
        cy.get('#status').contains('test7');
    });

    it('any tag with button role by text attr', () => {
        cy.aria('button.test8').first().click();
        cy.get('#status').contains('test8');
    });

    it('any tag with button role by aria-label attr', () => {
        cy.aria('button.test9').first().click();
        cy.get('#status').contains('test9');
    });
});
