describe('aria for button', () => {
    beforeEach(() => {
        cy.visit('cypress/integration/textbox.html');
    });

    it('can detect <input type=""> by "textbox" selector: without name', () => {
        cy.aria('textbox').first().click();
        cy.get('#status').contains('test1');
    });

    it('can detect <input type=""> by "textbox" selector: with name', () => {
        cy.aria('textbox.test2').click();
        cy.get('#status').contains('test2');
    });

    it('can detect <input type=""> by "textbox" selector: with aria-label', () => {
        cy.aria('textbox.test3').click();
        cy.get('#status').contains('test3');
    });

    it('can detect <input type="text"> by "textbox" selector: with name', () => {
        cy.aria('textbox.test4').click();
        cy.get('#status').contains('test4');
    });

    it('can detect <input type="date"> by "textbox" selector: with name', () => {
        cy.aria('textbox.test5').click();
        cy.get('#status').contains('test5');
        cy.aria('textbox.test5').type('2021-01-01');
    });

    it('can detect <input type="color"> by "textbox" selector: with name', () => {
        cy.aria('textbox.test6').click();
        cy.aria('textbox.test6').invoke('val', '#00ff00').trigger('change');
        cy.get('#status').contains('test6');
    });

    it('can detect <input type="datetime-local"> by "textbox" with: without name', () => {
        cy.aria('textbox.test7').click();
        cy.get('#status').contains('test7');
        cy.aria('textbox.test7').type('2021-01-01T07:07');
    });

    it('can detect <input type="datetime-local"> by "textbox" with: without name', () => {
        cy.aria('textbox.test8').click();
        cy.get('#status').contains('test8');
        cy.aria('textbox.test8').type('test7@example.com');
    });

    it('can detect <input type="month"> by "textbox" selector: with name', () => {
        cy.aria('textbox.test9').click();
        cy.get('#status').contains('test9');
        cy.aria('textbox.test9').type('2021-05');
    });

    it('can detect <input type="number"> by "textbox" selector: with name', () => {
        cy.aria('textbox.test10').click();
        cy.get('#status').contains('test10');
        cy.aria('textbox.test10').type('9876543210');
    });

    it('can detect <input type="password"> by "textbox" selector: with name', () => {
        cy.aria('textbox.test11').click();
        cy.get('#status').contains('test11');
        cy.aria('textbox.test11').type('passwordpassword');
    });

    it('can detect <input type="search"> by "textbox" selector: with name', () => {
        cy.aria('textbox.test13').click();
        cy.aria('textbox.test13').clear().type('search WAI-ARIA');
        cy.get('#status').contains('test13');
    });

    it('can detect <input type="tel"> by "textbox" selector: with name', () => {
        cy.aria('textbox.test14').click();
        cy.aria('textbox.test14').clear().type('119');
        cy.get('#status').contains('test14');
    });

    it('can detect <input type="time"> by "textbox" selector: with name', () => {
        cy.aria('textbox.test15').click();
        cy.aria('textbox.test15').type('03:03');
        cy.get('#status').contains('test15');
    });

    it('can detect <input type="url"> by "textbox" selector: with name', () => {
        cy.aria('textbox.test16').click();
        cy.aria('textbox.test16');
        cy.aria('textbox.test16')
            .invoke('val', 'https://future-architect.github.io/')
            .trigger('change');
        cy.get('#status').contains('test16');
    });

    it('can detect <input type="week"> by "textbox" selector: with name', () => {
        cy.aria('textbox.test17').click();
        cy.aria('textbox.test17').clear().type('2021-W10');
        cy.get('#status').contains('test17');
    });

    it('can detect <input> by "textbox" selector: with aria-labelled-by', () => {
        cy.aria('textbox.test18').click();
        cy.get('#status').contains('test18');
    });

    it('can detect <input> by "textbox" selector: with <label> tag', () => {
        cy.aria('textbox.test19').click();
        cy.get('#status').contains('test19');
    });

    it('can detect <textarea> by "textbox" selector: with aria-label', () => {
        cy.aria('textbox.test20').click();
        cy.aria('textbox.test20').clear().type('textare');
        cy.get('#status').contains('test20');
    });

    it('can detect <div contenteditable> by "textbox" selector: with aria-label and role', () => {
        cy.aria('textbox.test21').click();
        cy.aria('textbox.test21').clear().type('content editable');
        cy.get('#status').contains('test21');
    });
});
