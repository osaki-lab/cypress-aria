describe('aria for button', () => {
    beforeEach(() => {
        cy.visit('cypress/integration/other-form.html');
    });

    it('can detect <input type="range"> by "slider" selector: with name', () => {
        cy.aria('slider.test12').click();
        cy.aria('slider.test12').invoke('val', '1500').trigger('change');
        cy.get('#status').contains('test12');
    });

    it('can detect <input type="radio"> by "radio" selector: with name', () => {
        cy.aria('radio.test1').first().click();
        cy.get('#status').contains('test1-1');
    });

    it('can detect <input type="checkbox"> by "checkbox" selector: with text', () => {
        cy.aria('checkbox.test2').check();
        cy.get('#status').contains('test2');
    });

    it('can detect <input type="checkbox"> by "checkbox" selector: with parent label tag', () => {
        cy.aria('checkbox.test3').check();
        cy.get('#status').contains('test3');
    });

    it('can detect <input type="checkbox"> by "checkbox" selector: with label tag associated with for attribute', () => {
        cy.aria('checkbox.test3-2').check();
        cy.get('#status').contains('test3-2');
    });

    it('can detect <a> by "link" selector: with link text', () => {
        cy.aria('link.test4').click();
        cy.get('#status').contains('test4');
    });

    it('can detect <a> by "link" selector: with aria-label', () => {
        cy.aria('link.test5').click();
        cy.get('#status').contains('test5');
    });

    it('can detect <button> by "link" selector: with role and button text', () => {
        cy.aria('link.test6').click();
        cy.get('#status').contains('test6');
    });

    it('can detect <button> by "link" selector: with role and button image alt text', () => {
        cy.aria('link.test7').click();
        cy.get('#status').contains('test7');
    });
});
