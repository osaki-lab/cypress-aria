module.exports = {
    env: {
        browser: true,
        es2021: true,
        'cypress/globals': true,
    },
    extends: ['eslint:recommended', 'prettier', 'prettier/@typescript-eslint'],
    plugins: ['cypress'],
    parserOptions: {
        ecmaVersion: 12,
        sourceType: 'module',
    },
    rules: {},
};
