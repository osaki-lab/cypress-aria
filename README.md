# cypress-aria

cypress helper to use WAI-ARIA accessibility information

## Install with npm

```shell
npm install -D cypress-aria
```

## Install with Yarn

```shell
yarn add cypress-aria --dev
```

Then include in your project's `cypress/support/index.js`

```js
require('cypress-aria');
```

## Usage

After installation your `cy` object will have `aria` command. This provides similar behavior with `cy.get()`, but it is more flexible.

```js
it('finds button', () => {
    cy.aria('button.login').click();
});
```

Selector string is based on `"role.label"` instead of `"tag.class"`.

### Roles

WAI-ARIA defines [many roles](https://www.w3.org/TR/wai-aria-1.1/#roles_categorization). cypress-aria now supports the following roles:

-   `button`: `<button>`, `<input type="button">`, `<div role="button">` etc
-   `textbox`: `<input type="text">`, `<textarea>`, `<div role="textbox">` etc
-   `slider`: `<input type="range">`, `<div role="slider">`
-   `checkbox`: `<input type="checkbox">`, `<div role="checkbox">`
-   `radio`: `<input type="radio">`, `<div role="radio">`
-   `link`: `a`, `<button role="link">`

### Labels

WAI-ARIA defines two attributes to specifies labels: `aria-label`, `aria-labelledby`.

In addition to them, some tag have text node, `alt` attribute.

Form items have ability to get label from `name` attribute and `value` attribute (for buttons), parent's `<label>` tag.

### Examples

```html
<!-- match with "button.LOGIN" -->
<button>LOGIN</button>

<!-- match with "button.login" -->
<button type="button"><img src="/asset/key.png" alt="login" /></button>

<!-- match with "button.login" -->
<input type="button" value="login" />

<!-- match with "button.login" -->
<div role="button" aria-label="login">
    <img src="/asset/key.png" />
</div>

<!-- match with "textbox.login-password" -->
<input type="password" name="login-password" />

<!-- match with "textbox.login-password" -->
<label>login-password <input type="password" /></label>

<!-- match with "textbox.login-password" -->
<div id="password-label">login-password</div>
<input type="password" aria-labelledby="password-label" />
```

## License

This project is licensed under the terms of the [Apache 2 license](/LICENSE).
