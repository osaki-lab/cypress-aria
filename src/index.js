/// <reference types="cypress" />

// get aria-label and aria-labelledby
function searchAriaLabel(self, label) {
    if (self.attr('aria-label') === label) {
        return true;
    }
    const labelledBy = self.attr('aria-labelledby');
    if (labelledBy) {
        return Cypress.$(`#${labelledBy}`).text() === label;
    }
    return false;
}

// get parent's label
function searchFormLabel(self, label) {
    const parent = self.parent('label');
    if (parent.length > 0 && parent.text().trim() === label) {
        return true;
    }
    const id = self.attr('id');
    if (id && Cypress.$(`label[for=${id}]`).text() === label) {
        return true;
    }
    return false;
}

// get content's text and image's alt text
function searchContentLabel(self, label) {
    if (self.text().trim() === label) {
        return true;
    }
    const images = self.find(`img[alt="${label}"]`);
    return images.length > 0;
}

/**
 * Adds WAI-ARIA based element search support to Cypress using a custom command.
 *
 * @example
 ```js
 it('finds list items', () => {
    cy.aria('textbox.password')
      .should('have.length', 3)
  })
 ```
 */
async function aria(subject, selector, options = {}) {
    // options to log later
    const log = {
        name: 'aria',
        message: selector,
    };

    if (Cypress.dom.isElement(subject) && subject.length > 1) {
        throw new Error(
            'aria() can only be called on a single element. Your subject contained ' +
                subject.length +
                ' elements.'
        );
    }

    let [role, label = ''] = selector.split('.');
    label = label.trim();

    function findButton(contextNode, label) {
        let value = Cypress.$(
            'button, input[type="button"], input[type="submit"], input[type="reset"], input[type="image"], *[role="button"]',
            contextNode
        );
        if (label) {
            value = value.filter(function () {
                const self = Cypress.$(this);
                if (searchAriaLabel(self, label)) {
                    return true;
                }
                if (self.get(0).tagName === 'INPUT') {
                    for (const attr of ['name', 'value', 'alt']) {
                        if (self.attr(attr) === label) {
                            return true;
                        }
                    }
                    return false;
                }
                return searchContentLabel(self, label);
            });
        }
        return value;
    }
    function findTextBox(contextNode, label) {
        let value = Cypress.$(
            'input:not([type]), input[type="text"], input[type="color"], input[type="date"], input[type="datetime-local"], input[type="email"], input[type="month"], input[type="number"], input[type="password"], input[type="search"], input[type="tel"], input[type="time"], input[type="url"], input[type="week"], textarea, *[role="textbox"]',
            contextNode
        );
        if (label) {
            value = value.filter(function () {
                const self = Cypress.$(this);
                if (searchAriaLabel(self, label)) {
                    return true;
                }
                if (self.get(0).tagName === 'INPUT') {
                    if (self.attr('name') === label) {
                        return true;
                    }
                }
                return searchFormLabel(self, label);
            });
        }
        return value;
    }
    function findSlider(contextNode, label) {
        let value = Cypress.$(
            'input[type="range"], *[role="slider"]',
            contextNode
        );
        if (label) {
            value = value.filter(function () {
                const self = Cypress.$(this);
                if (searchAriaLabel(self, label)) {
                    return true;
                }
                if (self.get(0).tagName === 'INPUT') {
                    if (self.attr('name') === label) {
                        return true;
                    }
                }
                return searchFormLabel(self, label);
            });
        }
        return value;
    }
    function findRadio(contextNode, label) {
        let value = Cypress.$(
            'input[type="radio"], *[role="radio"]',
            contextNode
        );
        if (label) {
            value = value.filter(function () {
                const self = Cypress.$(this);
                if (searchAriaLabel(self, label)) {
                    return true;
                }
                if (self.get(0).tagName === 'INPUT') {
                    if (self.attr('name') === label) {
                        return true;
                    }
                }
                return searchFormLabel(self, label);
            });
        }
        return value;
    }

    function findCheckBox(contextNode, label) {
        let value = Cypress.$(
            'input[type="checkbox"], *[role="checkbox"]',
            contextNode
        );
        if (label) {
            value = value.filter(function () {
                const self = Cypress.$(this);
                if (searchAriaLabel(self, label)) {
                    return true;
                }
                if (self.get(0).tagName === 'INPUT') {
                    if (self.attr('name') === label) {
                        return true;
                    }
                }
                return searchFormLabel(self, label);
            });
        }
        return value;
    }

    function findLink(contextNode, label) {
        let value = Cypress.$('a, *[role="link"]', contextNode);
        if (label) {
            value = value.filter(function () {
                const self = Cypress.$(this);
                if (searchAriaLabel(self, label)) {
                    return true;
                }
                return searchContentLabel(self, label);
            });
        }
        return value;
    }

    const resolveValue = async () => {
        let contextNode;
        let withinSubject = cy.state('withinSubject');
        if (Cypress.dom.isElement(subject)) {
            contextNode = subject[0];
        } else if (Cypress.dom.isDocument(subject)) {
            contextNode = subject;
        } else if (withinSubject) {
            contextNode = withinSubject[0];
        } else {
            contextNode = cy.state('window').document;
        }
        let value;
        switch (role) {
            case 'button':
                value = findButton(contextNode, label);
                break;
            case 'textbox':
                value = findTextBox(contextNode, label);
                break;
            case 'slider':
                value = findSlider(contextNode, label);
                break;
            case 'radio':
                value = findRadio(contextNode, label);
                break;
            case 'checkbox':
                value = findCheckBox(contextNode, label);
                break;
            case 'link':
                value = findLink(contextNode, label);
        }
        return cy.verifyUpcomingAssertions(value, options, {
            onRetry: resolveValue,
        });
    };

    const result = await resolveValue();
    if (options.log !== false) {
        // TODO set found elements on the command log?
        Cypress.log(log);
    }
    return result;
}

Cypress.Commands.add(
    'aria',
    { prevSubject: ['optional', 'element', 'document'] },
    aria
);
